﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CalendarioPagos
{
    public partial class FramPrincipal : Form
    {
        public FramPrincipal()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void calendarioDePagosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmCalendario fgc = new FrmCalendario();
            fgc.MdiParent = this;
            fgc.Show();
        }
    }
}
