﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CalendarioPagos
{
    public partial class FrmCalendario : Form
    {
        private DataSet dsSistema;
        public FrmCalendario()
        {
            InitializeComponent();
        }
        public DataSet DsSistema
        {
            set
            {
                dsSistema = value;
            }
        }


        private void TextBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 8)
            {
                e.Handled = false;
                return;
            }
            bool IsDec = false;
            int nroDec = 0;
            for (int i = 0; i < textBox1.Text.Length; i++)
            {
                if (textBox1.Text[i] == '.')
                {
                    IsDec = true;
                }
                if (IsDec && nroDec++ >= 2)
                {
                    e.Handled = true;
                    return;
                }
            }

            if (e.KeyChar >= 48 && e.KeyChar <= 57)
            {
                e.Handled = false;
            }
            else if (e.KeyChar == 46)
            {
                e.Handled = (IsDec) ? true : false;
            }
            else
            {
                e.Handled = true;
            }

        }

        private void TextBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Para obligar a que sólo se introduzcan números
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else
              if (Char.IsControl(e.KeyChar)) //permitir teclas de control como retroceso
            {
                e.Handled = false;
            }
            else
            {
                //el resto de teclas pulsadas se desactivan
                e.Handled = true;
            }
        }

        private void TextBox3_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Para obligar a que sólo se introduzcan números
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else
              if (Char.IsControl(e.KeyChar)) //permitir teclas de control como retroceso
            {
                e.Handled = false;
            }
            else if(e.KeyChar ==37 ){
                e.Handled = false;
            }
            else
            {
                //el resto de teclas pulsadas se desactivan
                e.Handled = true;
            }
        }

        private void Button1_Click(object sender, EventArgs e)
        { 
            DataRow drFactura = dsSistema.Tables["Factura"].NewRow();
            drFactura["Fecha"] = DateTime.Now;

            FrmTabla frf = new FrmTabla
            {
                MdiParent = this.MdiParent
            };
            frf.Show();
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            FrmReporte frf = new FrmReporte
            {
                MdiParent = this.MdiParent
            };
            frf.Show();
        }
        
    }
    }

