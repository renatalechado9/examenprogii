﻿namespace CalendarioPagos
{
    partial class FrmTabla
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dgTabla = new System.Windows.Forms.DataGridView();
            this.metodoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.Nocuota = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Fecha = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Interes = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Principal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Cuota = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Saldo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgTabla)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.metodoBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // dgTabla
            // 
            this.dgTabla.AllowUserToOrderColumns = true;
            this.dgTabla.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgTabla.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Nocuota,
            this.Fecha,
            this.Interes,
            this.Principal,
            this.Cuota,
            this.Saldo});
            this.dgTabla.Location = new System.Drawing.Point(12, 12);
            this.dgTabla.Name = "dgTabla";
            this.dgTabla.Size = new System.Drawing.Size(803, 340);
            this.dgTabla.TabIndex = 0;
            // 
            // Nocuota
            // 
            this.Nocuota.DataPropertyName = "Nocuota";
            this.Nocuota.HeaderText = "NoCuota";
            this.Nocuota.Name = "Nocuota";
            // 
            // Fecha
            // 
            this.Fecha.DataPropertyName = "Fecha";
            this.Fecha.HeaderText = "Fecha";
            this.Fecha.Name = "Fecha";
            // 
            // Interes
            // 
            this.Interes.DataPropertyName = "Interes";
            this.Interes.HeaderText = "Interes";
            this.Interes.Name = "Interes";
            // 
            // Principal
            // 
            this.Principal.DataPropertyName = "Interes";
            this.Principal.HeaderText = "Principal";
            this.Principal.Name = "Principal";
            // 
            // Cuota
            // 
            this.Cuota.DataPropertyName = "Cuota";
            this.Cuota.HeaderText = "Cuota";
            this.Cuota.Name = "Cuota";
            // 
            // Saldo
            // 
            this.Saldo.DataPropertyName = "Saldo";
            this.Saldo.HeaderText = "Saldo";
            this.Saldo.Name = "Saldo";
            // 
            // FrmTabla
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(827, 364);
            this.Controls.Add(this.dgTabla);
            this.Name = "FrmTabla";
            this.Text = "FrmTabla";
            ((System.ComponentModel.ISupportInitialize)(this.dgTabla)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.metodoBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgTabla;
        private System.Windows.Forms.BindingSource metodoBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nocuota;
        private System.Windows.Forms.DataGridViewTextBoxColumn Fecha;
        private System.Windows.Forms.DataGridViewTextBoxColumn Interes;
        private System.Windows.Forms.DataGridViewTextBoxColumn Principal;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cuota;
        private System.Windows.Forms.DataGridViewTextBoxColumn Saldo;
    }
}