﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalendarioPagos
{
    public class Metodo
    {
        private int plazo;
        private double cuota;
        private int nocuota;
        private double monto;
        private double interes;
        private double saldo;
        private DateTime fecha;
        private DateTime fechadeinicio;

        public Metodo()
        {
        }

        public Metodo(int plazo, double cuota, int nocuota, double monto, double interes, double saldo, DateTime fecha, DateTime fechadeinicio)
        {
            this.plazo = plazo;
            this.cuota = cuota;
            this.nocuota = nocuota;
            this.monto = monto;
            this.interes = interes;
            this.saldo = saldo;
            this.fecha = fecha;
            this.fechadeinicio = fechadeinicio;
        }

        public double Saldo
        {
            get
            {
                return saldo;
            }

            set
            {
                saldo = value;
            }
        }
        public double Monto
        {
            get
            {
                return monto;
            }

            set
            {
                monto = value;
            }
        }
        public double Interes
        {
            get
            {
                return interes;
            }

            set
            {
                interes = value;
            }
        }
        public int Plazo
        {
            get
            {
                return plazo;
            }

            set
            {
                plazo= value;
            }
        }
        public int Nocuoita
        {
            get
            {
                return nocuota;
            }

            set
            {
               nocuota = value;
            }
        }
        public double Cuota
        {
            get
            {
                return cuota;
            }

            set
            {
                cuota= value;
            }
        }
        public DateTime Fechadeinicio
        {
            get
            {
                return fechadeinicio;
            }

            set
            {
                fechadeinicio = value;

            }
        }
        public DateTime Fecha
        {
            get
            {
                return fecha;
            }

            set
            {
                fecha = value;
            }
        }
    }
}
