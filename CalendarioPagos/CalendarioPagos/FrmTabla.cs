﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CalendarioPagos
{
    public partial class FrmTabla : Form
    {
        private DataSet dsFacturas;
        private BindingSource bsFacturas;

        public DataSet DsFacturas
        {
            get
            {
                return dsFacturas;
            }

            set
            {
                dsFacturas = value;
            }
        }
        public FrmTabla()
        {
            InitializeComponent();
        }
    }
}
